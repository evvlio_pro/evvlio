var total_cost = 0;

$(document).on('click', '.example-link', function () {
    var id = $(this).attr('id');
    $.fancybox.open({
        src: "examples/" + id + ".php",
        type: 'ajax',
        opts: {
            touch: false,
            autoFocus: false,
            clickSlide: false,
            keyboard: false,
            animationEffect: "zoom-in-out",
        },
    });
});

$(document).on('click', '.service_add', function () {
    var id = $(this).attr('service-id');
    var opp_button = document.getElementById("service_cancel_button_" + id);
    var curr_button = document.getElementById("service_add_button_" + id);
    opp_button.style.display = "inline-block";
    curr_button.style.display = "none";
    var service_price = document.getElementById("service_price_" + id).textContent;
    total_cost += parseInt(service_price);
    console.log(total_cost);
});

$(document).on('click', '.service_cancel', function () {
    var id = $(this).attr('service-id');
    var opp_button = document.getElementById("service_cancel_button_" + id);
    var curr_button = document.getElementById("service_add_button_" + id);
    opp_button.style.display = "none";
    curr_button.style.display = "inline-block";
    var service_price = document.getElementById("service_price_" + id).textContent;
    total_cost -= parseInt(service_price);
    console.log(total_cost);
});










  $("a.scrollto").click(function() {
    console.log("click");
    var elementClick = $(this).attr("href")
    var destination = $(elementClick).offset().top - 73;
    jQuery("html:not(:animated),body:not(:animated)").animate({
      scrollTop: destination
    }, 800);
    return false;
  });

  $(function() {
    $(window).scroll(function() {
      if($(this).scrollTop() != 0) {
        $('#to_top').fadeIn();
      } else {
        $('#to_top').fadeOut();
      }
    });
    $('#to_top').click(function() {
      $('body,html').animate({scrollTop:0},800);
    });
  });


// $(window).load(function(){
//     var $container = $('.portfolioContainer');
//     $container.isotope({
//         filter: '*',
//         animationOptions: {
//             duration: 750,
//             easing: 'linear',
//             queue: false
//         }
//     });
//
//     $('.portfolioFilter a').click(function(){
//         $('.portfolioFilter .current').removeClass('current');
//         $(this).addClass('current');
//
//         var selector = $(this).attr('data-filter');
//         $container.isotope({
//             filter: selector,
//             animationOptions: {
//                 duration: 750,
//                 easing: 'linear',
//                 queue: false
//             }
//          });
//          return false;
//     });
// });
