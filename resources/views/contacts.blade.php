@extends('template')

@section('body')
  <div class="main_banner_area  main_banner_area--min js-main_banner_area" style="background-image:url(img/bg_sect.jpg);min-height: 320px;padding: 90px 0 60px;background: #f9f9f9;">
    <div class="wrapper">
      <h1 class="h1_stripe h1_stripe_alt" style="
        font-weight: 300;
        text-align: center;">
        <div class="h_m_c_320">
          <span class="h1_stripe_substr h1_stripe_substr_alt">Контактная</span><br>
          <span class="h1_stripe_substr h1_stripe_substr_alt">информация</span>
        </div>
      </h1>
    </div>
  </div>

  <div class="container">
      <div class="row main_contacts_block">
         <div class="col-12 col-md-5 col-lg-5 col-xl-5">
            <h2 class="bottom_left_stripe">Контакты</h2>

            <div class="contacts_container">
              <div class="contacts_block">
                <div class="contacts_icon">
                  <i class="fas fa-phone"></i>
                </div>
                <div class="contacts_info">
                  8(017)352-65-45
                </div>
              </div>

              <div class="contacts_block">
                <div class="contacts_icon">
                  <i class="fas fa-mobile-alt"></i>
                </div>
                <div class="contacts_info">
                  +375(29)256-64-55
                </div>
              </div>

              <div class="contacts_block">
                <div class="contacts_icon">
                  <i class="fas fa-map-marker-alt"></i>
                </div>
                <div class="contacts_info">
                  <a class = "map_href" href="https://yandex.by/maps/-/CCcn6K0B" target="_blank">г.Гомель, ул. Межгалактическая, 69</a>
                </div>
              </div>

              <div class="contacts_block">
                <div class="contacts_icon">
                  <i class="far fa-clock"></i>
                </div>
                <div class="contacts_info">
                  Пн-Пт с 9:00 до 17:00
                </div>
              </div>
            </div>

            <h2 class="bottom_left_stripe">Грустно и одиноко?</h2>

            <div class="feedback_block">
               <form action="action.php">
                  <input class = "feedback_name" type="text" required placeholder="Как к Вам обратиться *">
                  <input class = "feedback_email" type="email" maxlength="64" required placeholder="Ваш E-mail *" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,}$">
                  <p><input class="feedback_submit" type="submit"></p>
               </form>
            </div>

            <h2 class="bottom_left_stripe">Мы в социальных сетях</h2>

            <div class="socials_block">
               <div class="vk"><i class="fab fa-vk"></i></div>
               <div class="fb"><i class="fab fa-facebook-f"></i></div>
               <div class="inst"><i class="fab fa-instagram"></i></div>
               <div class="yt"><i class="fab fa-youtube"></i></div>
            </div>

      </div>
      <div class="map_block col-12 col-md-7 col-lg-7 col-xl-7">
        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A2465b3fa0686e7bfb1f14e24156e3071e784863ddfdf05c1ba615643ac4926e0&amp;source=constructor" width="100%" height="350" frameborder="0"></iframe>
      </div>
  </div>
</div>
@endsection
