@extends('template')

@section('body')
  <div class="container">
      <div class="icon-menu icons_service">
          <h1>Услуги, которые мы предлагаем</h1>
          <p class="d">чтобы сделать вашу жизнь лучше и качественней</p>
          <ul class="icon-list my-4">
              <li>
                  <img src="/img/009-web-design-1.svg">
                  <a href="#web-design" class = "scrollto">
                      Web - дизайн </a>
              </li>
              <li>
                  <img src="/img/3d.svg">
                  <a href="#3d-modeling" class = "scrollto">
                      3D-моделирование </a>
              </li>
              <li>
                  <img src="/img/033-coding.svg">
                  <a href="#sites" class = "scrollto">
                      Разработка сайтов, верстка </a>
              </li>
              <li>
                  <img src="/img/printer.svg">
                  <a href="#polygraphy" class = "scrollto">
                      Полиграфия </a>
              </li>
              <li>
                  <img src="/img/021-consulting.svg">
                  <a href="#rek-usl" class = "scrollto">
                      Рекламные услуги </a>
              </li>
              <li>
                  <img src="/img/017-seo.svg">
                  <a href="#seo" class = "scrollto">
                      Услуги по продвижению </a>
              </li>
              <li>
                  <img src="/img/014-speed.svg">
                  <a href="#optimisation" class = "scrollto">
                      Оптимизация </a>
              </li>
              <li>
                  <img src="/img/018-copyright.svg">
                  <a href="#copyright" class = "scrollto">
                      Копирайтинг, рерайтинг, контент </a>
              </li>
              <li>
                  <img src="/img/translation.svg">
                  <a href="#translate" class = "scrollto">
                      Перевод </a>
              </li>
          </ul>
      </div>

      <div id = "web-design" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_left_stripe">Web-дизайн</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_preview">
                  <img class = "preview_img" src="img/examples/default.png" alt="Разработка брендбука" title="Разработка брендбука">
              </div>
          </div>
      </div>

      <div id = "3d-modeling" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
             <div class="example_preview">
               <img class = "preview_img" src="img/examples/default.png" alt="Разработка брендбука" title="Разработка брендбука">
             </div>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_right_stripe">3D-моделирование</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
      </div>

      <div id = "sites" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_left_stripe">Разработка сайтов, вёрстка</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_preview">
                  <img class = "preview_img" src="img/examples/default.png" alt="Разработка сайтов, вёрстка" title="Разработка сайтов, вёрстка">
              </div>
          </div>
      </div>

      <div id = "polygraphy" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
             <div class="example_preview">
               <img class = "preview_img" src="img/examples/default.png" alt="Полиграфия" title="Полиграфия">
             </div>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_right_stripe">Полиграфия</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
      </div>

      <div id = "rek-usl" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_left_stripe">Рекламные услуги</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_preview">
                  <img class = "preview_img" src="img/examples/default.png" alt="Рекламные услуги" title="Рекламные услуги">
              </div>
          </div>
      </div>

      <div id = "seo" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
             <div class="example_preview">
               <img class = "preview_img" src="img/examples/default.png" alt="Услуги по продвижению" title="Услуги по продвижению">
             </div>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_right_stripe">Услуги по продвижению</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
      </div>

      <div id = "optimisation" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_left_stripe">Оптимизация</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_preview">
                  <img class = "preview_img" src="img/examples/default.png" alt="Оптимизация" title="Оптимизация">
              </div>
          </div>
      </div>

      <div id = "copyright" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
             <div class="example_preview">
               <img class = "preview_img" src="img/examples/default.png" alt="Копирайтинг, рерайтинг, контент" title="Копирайтинг, рерайтинг, контент">
             </div>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_right_stripe">Копирайтинг, рерайтинг, контент</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
      </div>

      <div id = "translate" class="row service-block">
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_description">
                        <h2 class = "bottom_left_stripe">Перевод</h2>
                        <p>
                					 Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                				</p>
              </div>
              <button type="button" class="btn btn-primary btn-lg">Добавить в заказ</button>
          </div>
          <div class="col-12 col-md-6 col-lg-6 col-xl-6">
              <div class="example_preview">
                  <img class = "preview_img" src="img/examples/default.png" alt="Перевод" title="Перевод">
              </div>
          </div>
      </div>
  </div>
@endsection
