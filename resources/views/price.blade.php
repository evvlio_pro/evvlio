  @extends('template')

  @section('body')
    <?php
     $connect = mysqli_connect("a0311962.xsph.ru","a0311962_evvlio","PjnWqD6F", "a0311962_evvlio");
    ?>
    <section class="name-page text-left">
        <div class="container">
            <h1 class="name-page-heading">Сколько стоит?</h1>
            <p class="lead">Ниже приведена таблица наших услуг и цены в белорусских рублях. Для добавления услуги нажмите кнопку "Добавить" </p>
        </div>
    </section>
    <div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
        <div class="py-3 bg-light">
            <div class="container">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                <?php
                 $cat_select = "SELECT * FROM categories ORDER BY weight ASC";
                 $cat_result = mysqli_query($connect, $cat_select);
                 $tab_text = '';
                 $is_tab_active = '';
                 $is_aria_selected = '';
                 $is_need_active_tab = 1;
                 while ($cat_row = mysqli_fetch_array($cat_result)) {
                   if ($is_need_active_tab == 1) {
                     $is_nav_active = "active";
                     $is_tab_active = "show active";
                     $is_aria_selected = "true";
                     $is_need_active_tab = 0;
                   } else {
                     $is_nav_active = "";
                     $is_tab_active = "";
                     $is_aria_selected = "false";
                   }
                   echo '<li class="nav-item">
                                <a class="nav-link '.$is_nav_active.'" id="'.$cat_row["category_id"].'-tab" data-toggle="tab" href="#'.$cat_row["category_id"].'" role="tab" aria-controls="'.$cat_row["category_id"].'" aria-selected="'.$is_aria_selected.'">'.$cat_row["category_name"].'</a>
                         </li>';
                   $tab_text .= '<div class="tab-pane fade '.$is_tab_active.'" id="'.$cat_row["category_id"].'" role="tabpanel" aria-labelledby="'.$cat_row["category_id"].'-tab">
                                     <div class="row">
                                         <div class="py-3 w-100">';
                                            $sub_category_sql = "SELECT * FROM sub_categories WHERE parent_category = '".$cat_row["category_id"]."' ORDER BY sub_category_name DESC";
                                            $sub_category_result = mysqli_query($connect, $sub_category_sql);
                                            while ($sub_category_row = mysqli_fetch_array($sub_category_result)) {
                                              $tab_text .= '<div class="pre_category my-2 fs1_5">'.$sub_category_row["sub_category_name"].'</div>';
                                              $pos_sql = "SELECT * FROM service WHERE sub_category = '".$sub_category_row["sub_category_id"]."' ORDER BY weight ASC";
                                              $pos_result = mysqli_query($connect, $pos_sql);
                                              while ($pos_row = mysqli_fetch_array($pos_result)) {
                                                 $tab_text .= '<div class="position_line px-2">
                                                                   <div class="title_row p-2">'.$pos_row["service_name"].'</div>
                                                                     <div class="example_row p-2 text-center">';
                                                    if ($pos_row["example"] != "") {
                                                        $tab_text .= '<button type="button" id = "'.$pos_row["example"].'" class="btn btn-link example-link">Пример</button>';
                                                    }
                                                 $tab_text .=        '</div>
                                                                   <div id = "service_price_'.$pos_row["service_id"].'" class="price_row p-2 text-center">'.$pos_row["cost_BYN"].'</div>
                                                                   <div class="buy_row p-2 text-center">
                                                                           <button id = "service_add_button_'.$pos_row["service_id"].'" service-id = "'.$pos_row["service_id"].'" type="button" class=" service_add btn btn-outline-primary btn-sm"><i class="fas fa-cart-arrow-down"></i> Заказать</button>
                                                                           <button id = "service_cancel_button_'.$pos_row["service_id"].'" service-id = "'.$pos_row["service_id"].'" type="button" class="service_cancel btn btn-outline-danger btn-sm"><i class="fas fa-times"></i></i> Убрать</button></div>
                                                                    </div>';
                                              }
                                            }
                   $tab_text.= '         </div>
                                     </div>
                                 </div>';
                 }
               ?>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <?php echo $tab_text; ?>
                </div>

            </div>
        </div>
    </div>
  @endsection
