<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('start');
});

Route::get('price', function () {
    return view('price');
});

Route::get('uslugi', function () {
    return view('uslugi');
});

Route::get('contacts', function () {
    return view('contacts');
});
